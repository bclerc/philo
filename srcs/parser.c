/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bclerc <bclerc@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/09 16:29:15 by bclerc            #+#    #+#             */
/*   Updated: 2021/09/29 11:53:54 by bclerc           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/philosophers.h"

int	check_integer(char *argv)
{
	int	i;

	i = 0;
	while (argv[i])
	{
		if (!ft_isdigit(argv[i]))
			return (0);
		i++;
	}
	return (1);
}

int	parse_args(t_info *info, char **str)
{
	if (check_integer(str[0]) && !(ft_atol(str[0]) > INT_MAX))
		info->number_philo = ft_atol(str[0]);
	else
		return (0);
	if (check_integer(str[1]) && !(ft_atol(str[1]) > INT_MAX))
		info->time_to_die = ft_atol(str[1]);
	else
		return (0);
	if (check_integer(str[2]) && !(ft_atol(str[2]) > INT_MAX))
		info->time_to_eat = ft_atol(str[2]);
	else
		return (0);
	if (check_integer(str[3]) && !(ft_atol(str[3]) > INT_MAX))
		info->time_sleep = ft_atol(str[3]);
	else
		return (0);
	if (str[4])
	{
		if (check_integer(str[4]) && !(ft_atol(str[4]) > INT_MAX))
			info->eat_limit = ft_atol(str[4]);
		else
			return (0);
	}
	return (1);
}
