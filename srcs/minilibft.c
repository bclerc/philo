/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minilibft.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bclerc <bclerc@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/28 12:39:10 by bclerc            #+#    #+#             */
/*   Updated: 2021/10/07 14:01:20 by bclerc           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/philosophers.h"

void	philo_write(char *msg, t_philo *philo, struct timeval time)
{
	int	id;

	if (!protected_exit(philo->info))
	{
		id = philo->id + 1;
		printf("%u %d %s\n", time_lapse(time), id, msg);
	}
}

t_info	*init_info(char **argv)
{
	struct timeval	date;
	t_info			*info;

	gettimeofday(&date, NULL);
	info = malloc(sizeof(t_info));
	if (!info)
		return (NULL);
	info->eat_limit = -1;
	if (!parse_args(info, &argv[1]))
	{
		free(info);
		exit_error_usage();
		return (NULL);
	}
	info->time = date;
	info->exit = 0;
	pthread_mutex_init(&info->exit_mutex, NULL);
	return (info);
}

int	ft_isdigit(int c)
{
	if (c >= '0' && c <= '9')
		return (1);
	return (0);
}

long long	check_neg(int neg, long long value)
{
	if (neg)
		return (-value);
	return (value);
}

long	ft_atol(const char *str)
{
	unsigned int	i;
	int				neg;
	long long		b;

	i = 0;
	neg = 0;
	while (str[i] && (str[i] == '\r' || str[i] == '\t' || str[i] == ' '
			|| str[i] == '\v' || str[i] == '\f' || str[i] == '\n'))
		i++;
	if (str[i] == '+' || str[i] == '-')
	{
		if (str[i] == '-')
			neg = 1;
		i++;
	}
	b = 0;
	while (ft_isdigit(str[i]))
	{
		b = (b * 10) + (str[i] - '0');
		i++;
	}
	return (check_neg(neg, b));
}
