/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bclerc <bclerc@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/09 16:26:49 by bclerc            #+#    #+#             */
/*   Updated: 2021/10/07 13:59:12 by bclerc           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/philosophers.h"

void	usleep_shit(unsigned int time_wait, t_info *info)
{
	struct timeval	time;

	gettimeofday(&time, NULL);
	while (time_lapse(time) < time_wait)
	{
		if (protected_exit(info))
			break ;
		usleep(50);
	}
}

unsigned int	time_lapse(struct timeval time)
{
	struct timeval	current;

	gettimeofday(&current, NULL);
	return (((current.tv_sec * 1000000 + current.tv_usec)
			- (time.tv_sec * 1000000 + time.tv_usec)) / 1000);
}

int	have_min_eat(t_philo *philos)
{
	int	must_eat;
	int	ret;
	int	nb;
	int	i;

	must_eat = philos[0].info->eat_limit;
	nb = philos[0].info->number_philo;
	i = 0;
	if (must_eat < 0)
		return (0);
	while (i < nb)
	{
		pthread_mutex_lock(&philos[i].eat_mutex);
		ret = philos->eat;
		pthread_mutex_unlock(&philos[i].eat_mutex);
		if (ret <= must_eat)
			return (0);
		i++;
	}
	return (1);
}

void	exit_error_usage(void)
{
	printf("Error: Bad arguments\n");
	printf("Usage: /philosophers <number of philosophers> <time to die> ");
	printf("<time to eat> <time to sleep> ");
	printf("[number of times each philosophers mut eat]\n");
}

void	exit_philo(t_philo *philos, t_info *info)
{
	int	i;

	i = -1;
	sleep(1);
	free(philos);
	free(info->threads);
	free(info->fork);
	free(info);
}
