/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philosophers.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bclerc <bclerc@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/09 16:18:02 by bclerc            #+#    #+#             */
/*   Updated: 2021/10/07 14:04:53 by bclerc           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/philosophers.h"

t_fork	*init_fork(int nb)
{
	t_fork	*fork;
	int		i;

	i = 0;
	fork = malloc(sizeof(t_fork) * nb);
	if (!fork)
		return (NULL);
	while (i < nb)
	{
		pthread_mutex_init(&fork[i].fork, NULL);
		i++;
	}
	return (fork);
}

void	*routine(void *data)
{
	t_philo	*philo;

	philo = (t_philo *) data;
	if (philo->id % 2 == 1)
		usleep_shit(1, philo->info);
	while (!protected_exit(philo->info))
	{
		if (!eat(philo))
			break ;
		if (!think(philo))
			break ;
	}
	return (NULL);
}

t_philo	createphilo(int id, t_info *info)
{
	struct timeval	current;
	t_philo			philo;

	gettimeofday(&current, NULL);
	pthread_mutex_init(&philo.eat_mutex, NULL);
	philo.last_meal = current;
	philo.info = info;
	philo.id = id;
	philo.eat = 0;
	philo.state = 0;
	return (philo);
}

t_philo	*execute_philo(t_info *info, int nb)
{
	t_philo		*philo;
	pthread_t	*threads;
	int			i;

	threads = malloc(sizeof(pthread_t) * nb);
	philo = malloc(sizeof(t_philo) * nb);
	i = 0;
	info->threads = threads;
	while (i < nb)
	{
		philo[i] = createphilo(i, info);
		philo[i].parent = philo;
		philo[i].fork = &info->fork[i];
		if (i == nb - 1)
			philo[i].r_philo = &philo[0];
		else
			philo[i].r_philo = &philo[i + 1];
		i++;
	}
	i = -1;
	while (++i < nb)
		pthread_create(&threads[i], NULL, routine, &philo[i]);
	return (philo);
}

int	main(int argc, char **argv)
{
	t_philo			*philo;
	t_info			*info;
	int				i;

	if (argc != 5 && argc != 6)
	{
		exit_error_usage();
		return (-1);
	}
	info = init_info(argv);
	if (!info)
		return (-1);
	info->fork = init_fork(info->number_philo);
	pthread_mutex_init(&info->print, NULL);
	philo = execute_philo(info, info->number_philo);
	if (info->eat_limit > -1)
	{
		while (!protected_exit(info))
			check_eat(philo, info);
	}
	i = -1;
	while (++i < info->number_philo)
		pthread_join(info->threads[i], NULL);
	exit_philo(philo, info);
	return (0);
}
