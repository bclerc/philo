/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   instruct.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bclerc <bclerc@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/27 12:32:16 by bclerc            #+#    #+#             */
/*   Updated: 2021/10/07 14:16:20 by bclerc           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/philosophers.h"

void	*isdeath(void *data)
{
	t_philo	*philo;

	philo = (t_philo *)data;
	usleep_shit(philo->info->time_to_die + 1, philo->info);
	if (protected_exit(philo->info))
		return (0);
	if (time_lapse(philo->last_meal) >= (unsigned int) philo->info->time_to_die)
	{
		pthread_mutex_lock(&philo->info->print);
		if (philo->state == 1)
		{
			pthread_mutex_unlock(&philo->info->print);
			return (0);
		}
		philo_write("died", philo, philo->info->time);
		pthread_mutex_lock(&philo->info->exit_mutex);
		philo->info->exit = 1;
		pthread_mutex_unlock(&philo->info->exit_mutex);
		pthread_mutex_unlock(&philo->info->print);
	}
	return (0);
}

int	think(t_philo *philo)
{
	int	exit;

	pthread_create(&philo->death_thread, NULL, isdeath, philo);
	pthread_detach(philo->death_thread);
	pthread_mutex_lock(&philo->info->print);
	philo_write("is thinking", philo, philo->info->time);
	pthread_mutex_unlock(&philo->info->print);
	exit = !protected_exit(philo->info);
	return (exit);
}

int	_sleep(t_philo *philo)
{
	struct timeval	current;
	int				exit;

	pthread_create(&philo->death_thread, NULL, isdeath, philo);
	pthread_detach(philo->death_thread);
	pthread_mutex_lock(&philo->info->print);
	pthread_mutex_unlock(&philo->info->fork[philo->id].fork);
	pthread_mutex_unlock(&philo->info->fork[philo->r_philo->id].fork);
	philo_write("is sleeping", philo, philo->info->time);
	pthread_mutex_unlock(&philo->info->print);
	pthread_mutex_lock(&philo->eat_mutex);
	philo->eat++;
	pthread_mutex_unlock(&philo->eat_mutex);
	usleep_shit(philo->info->time_sleep, philo->info);
	exit = !protected_exit(philo->info);
	return (exit);
}

void	take_fork(t_philo *philo)
{
	pthread_mutex_t	*l_fork;
	pthread_mutex_t	*r_fork;

	l_fork = &philo->info->fork[philo->id].fork;
	r_fork = &philo->info->fork[philo->r_philo->id].fork;
	if (philo->id == philo->info->number_philo - 1)
	{
		l_fork = &philo->info->fork[philo->r_philo->id].fork;
		r_fork = &philo->info->fork[philo->id].fork;
	}
	pthread_mutex_lock(l_fork);
	pthread_mutex_lock(&philo->info->print);
	philo_write("has taken a fork", philo, philo->info->time);
	pthread_mutex_unlock(&philo->info->print);
	pthread_mutex_lock(r_fork);
	pthread_mutex_lock(&philo->info->print);
	philo_write("has taken a fork", philo, philo->info->time);
	philo->state = 1;
	philo_write("is eating", philo, philo->info->time);
	pthread_mutex_unlock(&philo->info->print);
}

int	eat(t_philo *philo)
{
	int				exit;
	struct timeval	current;

	pthread_create(&philo->death_thread, NULL, isdeath, philo);
	pthread_detach(philo->death_thread);
	if (philo->info->number_philo == 1)
	{
		pthread_mutex_lock(&philo->info->print);
		philo_write("has taken a fork", philo, philo->info->time);
		pthread_mutex_unlock(&philo->info->print);
		usleep(philo->info->time_to_die);
		return (0);
	}
	take_fork(philo);
	gettimeofday(&current, NULL);
	philo->last_meal = current;
	pthread_mutex_lock(&philo->info->print);
	philo->state = 0;
	pthread_mutex_unlock(&philo->info->print);
	usleep_shit(philo->info->time_to_eat, philo->info);
	return (_sleep(philo));
}
