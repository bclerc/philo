/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mutex.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bclerc <bclerc@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/29 17:38:30 by bclerc            #+#    #+#             */
/*   Updated: 2021/10/07 14:05:51 by bclerc           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/philosophers.h"

void	protected_eat(t_philo *philo)
{
	pthread_mutex_lock(&philo->eat_mutex);
	philo->eat++;
	pthread_mutex_unlock(&philo->eat_mutex);
}

int	protected_exit(t_info *info)
{
	int	ret;

	pthread_mutex_lock(&info->exit_mutex);
	ret = info->exit;
	pthread_mutex_unlock(&info->exit_mutex);
	return (ret);
}

void	check_eat(t_philo *philos, t_info *info)
{
	if (have_min_eat(philos))
	{
		pthread_mutex_lock(&info->print);
		printf("All philosophers have eat %d time(s)\n",
			info->eat_limit);
		pthread_mutex_lock(&info->exit_mutex);
		info->exit = 1;
		pthread_mutex_unlock(&info->exit_mutex);
		pthread_mutex_unlock(&info->print);
	}
	usleep_shit(100, info);
}
