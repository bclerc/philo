/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philosophers.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bclerc <bclerc@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/09 16:17:00 by bclerc            #+#    #+#             */
/*   Updated: 2021/10/07 14:05:52 by bclerc           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILOSOPHERS_H
# define PHILOSOPHERS_H

# include <unistd.h>
# include <stdio.h>
# include <stdio.h>
# include <stdlib.h>
# include <pthread.h>
# include <sys/time.h>
# define INT_MAX 2147483647

typedef struct s_fork
{
	pthread_mutex_t	fork;
}				t_fork;

typedef struct s_info
{
	pthread_mutex_t	print;
	pthread_mutex_t	exit_mutex;
	pthread_t		*threads;
	struct timeval	time;
	t_fork			*fork;
	int				exit;
	int				number_philo;
	int				time_to_die;
	int				time_to_eat;
	int				time_sleep;
	int				eat_limit;

}				t_info;			

typedef struct s_philo
{
	struct timeval	last_meal;
	struct s_philo	*parent;
	struct s_philo	*r_philo;
	pthread_mutex_t	eat_mutex;
	pthread_t		death_thread;
	pthread_t		thread;
	t_fork			*fork;
	t_info			*info;
	int				id;
	int				eat;
	int				state;
}				t_philo;

t_info			*init_info(char **argv);
unsigned int	time_lapse(struct timeval time);
long			ft_atol(const char *str);
int				ft_isdigit(int c);
int				have_min_eat(t_philo *philos);
int				check_integer(char *argv);
int				think(t_philo *philo);
int				_sleep(t_philo *philo);
int				eat(t_philo *philo);
int				parse_args(t_info *info, char **str);
int				protected_exit(t_info *info);
int				protected_get_eat(t_philo *philo);
void			*isdeath(void *data);
void			exit_philo(t_philo *philos, t_info *info);
void			philo_write(char *msg, t_philo *philo, struct timeval time);
void			usleep_shit(unsigned int time_wait, t_info *info);
void			exit_error_usage(void);
void			protected_eat(t_philo *philo);
void			check_eat(t_philo *philos, t_info *info);
#endif